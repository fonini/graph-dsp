#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


from setuptools import setup, find_packages
from codecs import open
import pathlib


NAME = 'graphdsp'
DESCRIPTION = 'Graph Signal Processing'
URL = 'https://github.com/fofoni/graphdsp'
AUTHOR = 'Pedro Angelo'
EMAIL = 'pedro.fonini@smt.ufrj.br'
REQUIRES_PYTHON = '>=3.6.0'
VERSION_TUPLE = (0, 0, 1)
VERSION_EXTRA = 'a+'

REQUIRED = [
    'matplotlib',
    'numpy',
    'scipy',
]

REQUIRED_DEV = [
    'flake8',
    'tox',
]


here = pathlib.Path(__file__).resolve().parent

# Get the long description from the README file
with open(here/'README.rst', encoding='utf-8') as f:
    long_description = '\n' + f.read()

setup(
    name=NAME,
    version='.'.join(map(str, VERSION_TUPLE)) + VERSION_EXTRA,
    description=DESCRIPTION,
    long_description=long_description,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    author=AUTHOR,
    author_email=EMAIL,
    classifiers=[
        'Intended Audience :: Science/Research',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Scientific/Engineering',
    ],
    keywords=[
        'signal-processing',
        'graphs',
        'dsp',
        'academic',
    ],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=REQUIRED,
    extras_require={'dev': REQUIRED_DEV},
)
