#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


"""
Graph Signal Processing

This is the Graph Signal Processing package, developed by me as part of my
M.Sc. program.
"""


from . import pltcfg

from .core import (
    GraphBadParametersError, GraphInconsistencyError,
    UndirectedWeightedGraph, GeometricUWG,
    GraphSignal, GraphSignalSpectrum, GraphSignalArray
)
