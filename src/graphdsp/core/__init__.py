#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


"""
Graph Signal Processing: core tools

The 'core' package contains modules that implement the basic graph signal
processing tools.
"""


from .exceptions import (
    GSPError,
    GraphBadParametersError, GraphInconsistencyError
)
from .graphs import UndirectedWeightedGraph, GeometricUWG
from .signals import GraphSignal, GraphSignalSpectrum, GraphSignalArray
