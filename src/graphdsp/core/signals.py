#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


"""
Signals module for the Graph Signal Processing package

This module implements classes for representing and operating on graph signals,
i.e., signals defined on graphs.
"""


import abc

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from .exceptions import GraphBadParametersError, GraphInconsistencyError


""" TODO:

* Make GraphArray class, with a np.2darray base. This class should support
    (X) only time-domain representation (with gft on-demand, and read/write
          access to the base array)
    ( ) both time- and freq- representation (read-only)
    ( ) multi-dimensional
    ( ) Split GraphSignalArray into Base, Signal, Spectrum (just like
          GraphSignal).
    ( ) Gather duplicate code in GraphSignalArrayBase and GraphSignalBase into
          a GraphNdSignalBase common base.
    ( ) granular (some indexes are read-only, and some are not)
    ( ) slicing
    ( ) returning a GraphSignal or -Spectrum, or transform pair, given an
          index or iterable of indices


* Implement py36/37 in tox

"""


class GraphSignalBase(metaclass=abc.ABCMeta):
    """Base class for graph-domain and frequency-domain graph signals.

    This is an abstract base class. An instance of this class is either a
    graph-domain graph signal (usually just called a "graph signal") or a
    frequency-domain graph signal (usually called a "frequency-domain
    representation" of a graph signal).

    Signature:
    super().__init__(values,
                     graph=None,
                     copy_values=False,
                     from_transform=None)

    parameters:
    values: a vector with the values of the signal at each node.
    graph: the graph on which the signal is defined (optinal).
    copy_values: whether or not to ensure that the original memory of the
        'values' parameter will not be touched.
    from_transform: a transformed signal, if it is known
    """

    def __init__(self, values=None, graph=None,
                 copy_values=True, from_transform=None, mutable=None):

        if graph is not None:
            if values is None:
                values = np.zeros((len(graph),))
            if len(graph) != len(values):
                raise GraphBadParametersError(
                    "The number of values must be equal to the number of "
                    "nodes in the graph.")
        else:
            if values is None:
                raise GraphBadParametersError(
                    "To initialize a GraphSignal, you must provide at least "
                    "one of the parameters `values' or `graph'.")
        self._graph = graph

        self._values = np.asarray(values, dtype=float)
        if copy_values:
            self._values = self._values.copy()

        if from_transform is not None:
            if mutable:
                raise GraphBadParametersError(
                    "The `from_transform' parameter is for tying the graph "
                    "signal object with a known transform object. This is "
                    "incompatible with making the objects mutable.")
            mutable = False
            if not isinstance(from_transform, self._transform_class):
                from_transform = self._transform_class(
                    from_transform, mutable=False)
            else:
                if from_transform._transform_is_cached:
                    raise ValueError(
                        "Attempted to use as known transform a signal object "
                        "which already has a transform. You should have used "
                        "this transform object instead of constructing a new "
                        "signal object.")
                if from_transform.is_mutable():
                    from_transform.freeze()
            if from_transform.graph is None:
                from_transform.graph = self.graph
            else:
                if self.graph is None:
                    self.graph = from_transform.graph
                if from_transform.graph is not self.graph:
                    raise ValueError(
                        "Attempted to use as known transform a transform "
                        "object from a different graph.")
            self._transform = from_transform
            self._transform_is_cached = True
            self._transform._transform = self
            self._transform._transform_is_cached = True
        else:
            if mutable is None:
                mutable = True
            self._transform_is_cached = False

        self._values.flags['WRITEABLE'] = mutable

    def __repr__(self):
        props = []
        if self.graph is not None:
            props.append(('graph', hex(id(self.graph))))
        props.append(('values', repr(self.values)))
        props.append(('transform_is_cached', repr(self._transform_is_cached)))
        props.append(('mutable', repr(self.is_mutable())))
        return "<{}.{} with:\n{}>".format(
            self.__class__.__module__,
            self.__class__.__name__,
            ''.join([
                " {}={},\n".format(prop_name, prop_val)
                for prop_name, prop_val in props
            ])
        )

    def __len__(self):
        return len(self._values)

    def __iter__(self):
        return iter(self._values)

    def __getitem__(self, i):
        return self._values[i]

    def __setitem__(self, i, value):
        # We just trust that numpy will raise an exception if self is not
        # mutable.
        self._values[i] = value

    def _binary_op(self, other, array_op, transform_op=None):
        cls = type(self)
        extra_args = {}

        try:
            other_graph = other.graph
        except AttributeError:
            other_graph = None

        if self.graph is not None:
            graph = self.graph
            if other_graph is not None and other_graph is not graph:
                raise GraphInconsistencyError(
                    "Binary operations between graph signals should not act"
                    " on signals defined for different graphs.")
        else:
            graph = other_graph

        if (transform_op is not None and
            self._transform_is_cached and
            hasattr(other, '_transform_is_cached') and
            other._transform_is_cached
        ):
            if transform_op == 'same':
                transform_op = array_op
            extra_args['from_transform'] = transform_op(
                other._get_transform()._values, self._get_transform()._values)

        return cls(array_op(self._values, np.asarray(other)), graph=graph,
                   copy_values=False, mutable=self.is_mutable(),
                   **extra_args)

    def __add__(self, other):
        return self._binary_op(other, lambda x, y: x + y, transform_op='same')
    __radd__ = __add__

    def __sub__(self, other):
        return self._binary_op(other, lambda x, y: x - y, transform_op='same')

    def __rsub__(self, other):
        return self._binary_op(other, lambda x, y: y - x, transform_op='same')

    def __mul__(self, other):
        return self._binary_op(other, lambda x, y: x * y)
    __rmul__ = __mul__

    def __div__(self, other):
        return self._binary_op(other, lambda x, y: x / y)

    def __rdiv__(self, other):
        return self._binary_op(other, lambda x, y: y / x)

    def __matmul__(self, other):
        return self._values @ np.asarray(other)

    def __rmatmul__(self, other):
        return np.asarray(other) @ self._values

    def norm(self):
        return np.linalg.norm(self._values)

    def is_mutable(self):
        return self._values.flags['WRITEABLE']

    def freeze(self):
        del self._transform_property
        self._values.flags['WRITEABLE'] = False

    def unfreeze(self):
        self._values.flags['WRITEABLE'] = True

    @property
    def graph(self):
        """The graph object of the graph subjacent to this signal.

        A graph signal object might not have a graph attached to it. In this
        case, this property is None. This property can be changed by
        assignment:

            signal.graph = new_graph

        Deleting this property (del signal.graph) returns it to being None.

        Both assigning to and deleting the property have the side-effect of
        invalidating the cached transform of the signal.
        """
        return self._graph

    @graph.setter
    def graph(self, new_graph):
        if new_graph is not None and len(new_graph) != len(self):
            raise ValueError(
                "The number of nodes in the graph must be equal to the number "
                "of signal values.")
        del self.graph  # call the deleter
        self._graph = new_graph

    @graph.deleter
    def graph(self):
        self._del_transform()
        self._graph = None

    @classmethod
    def make_transform_pair(cls, values, transformed_values, graph=None):
        x = cls(values, graph, from_transform=transformed_values,
                mutable=False)
        return x, x._transform

    @property
    def values(self):
        return self._values

    def max(self):
        return self.values.max()

    def min(self):
        return self.values.min()

    def _get_transform(self):
        if self._transform_is_cached:
            assert not self.is_mutable()
            return self._transform
        if self._graph is None:
            raise ValueError("You can only calculate the graph Fourier "
                             "transform of a signal whose graph is known.")
        transform = self._calc_transform()
        if not self.is_mutable():
            self._set_transform(transform)
        return transform

    def _del_transform(self):
        if self._transform_is_cached:
            self._transform_is_cached = False
            del self._transform

    @abc.abstractmethod
    def _calc_transform(self):
        pass

    def _set_transform(self, transform):
        self._transform_is_cached = True
        self._transform = transform

    def break_reference_loop(self):
        if self._transform_is_cached:
            if self._transform._transform_is_cached:
                self._transform._del_transform()
                self._del_transform()

    @property
    def _transform_property(self):
        return self._get_transform()

    @_transform_property.deleter
    def _transform_property(self):
        self._del_transform()


class GraphSignal(GraphSignalBase):
    """Graph signal

    A signal defined on a graph.

    Signature:
    GraphSignal(values, graph=None)

    See 'GraphSignalBase' for an explanation of the parameters.
    """

    gft = GraphSignalBase._transform_property
    gft.__doc__ = (
        """Graph Fourier transform.

        This property is cached (if it has been already fetched, the cached
        value is returned, otherwise it is calculated, cached, and returned).
        To delete the cached value, just do 'del signal.gft'.

        To access this property, the subjacent graph must be set (see the
        'graph' property).
        """)

    def _calc_transform(self):
        return self._graph.gft(self)

    def plot(self, *,
            cmap=mpl.cm.get_cmap('viridis'), title=None,
            colorbar=True, edge_color='k',
            show_indexes=False, filename=None, dpi=300,
            dotsize=100, dotcolor=None,
            vminmax=None, highlight=None):
        points = self.graph.nodes
        W = self.graph.W
        N = len(self)
        fig, ax = plt.subplots(frameon=False)
        ax.axis('off')
        for i in range(N):
            for j in range(i+1,N):
                if W[i,j] > 0:
                    extra_args = {'zorder': 1}
                    if edge_color == 'grayscale':
                        extra_args.update(color=str(1-W[i,j]/W.max()))
                    elif edge_color == 'zero-crossing':
                        if self.values[i] * self.values[j] < 0:
                            extra_args.update(color='r', zorder=2)
                        else:
                            extra_args.update(linestyle=':', color='0.7',
                                              zorder=1)
                    else:
                        extra_args.update(color=edge_color)
                    ax.plot(points[[i,j],0], points[[i,j],1],
                            **extra_args)
        extra_args = {}
        if dotcolor is None:
            extra_args.update(c=self.values)
        else:
            extra_args.update(c=dotcolor)
        if vminmax is not None:
            extra_args.update(vmin=-vminmax, vmax=vminmax)
        if highlight is not None:
            ax.scatter(points[highlight,0], points[highlight,1],
                       s=int(2.5*dotsize), zorder=3,
                       c=[(1.0, 0.0, 0.0, 0.5)])
        scatter = ax.scatter(points[:,0], points[:,1],
                             s=dotsize, lw=1, edgecolor='k',
                             cmap=cmap, zorder=4,
                             **extra_args)
        if show_indexes:
            for i in range(N):
                ax.text(points[i,0]+.015, points[i,1]+.015, str(i))
        if colorbar:
            cbar = fig.colorbar(scatter)
        ax.set_aspect('equal')
        if title is not None:
            #ax.set_title(title, fontsize=gsp.pltcfg.dfs)
            ax.set_title(title)
        ax.set_xticks([])
        ax.set_yticks([])
        #fig.tight_layout()
        if filename:
            fig.savefig(filename, dpi=dpi, bbox_inches='tight')


class GraphSignalSpectrum(GraphSignalBase):
    """Graph signal spectrum

    The Fourier transform of a signal defined on a graph.

    Signature:
    GraphSignalSpectrum(values, graph=None)

    See 'GraphSignalBase' for an explanation of the parameters.
    """

    igft = GraphSignalBase._transform_property
    igft.__doc__ = (
        """Inverse graph Fourier transform.

        This property is cached (if it has been already fetched, the cached
        value is returned, otherwise it is calculated, cached, and returned).
        To delete the cached value, just do 'del signal.igft'.

        To access this property, the subjacent graph must be set (see the
        'graph' property).
        """)

    def _calc_transform(self):
        return self._graph.igft(self)


GraphSignal._transform_class = GraphSignalSpectrum
GraphSignalSpectrum._transform_class = GraphSignal


class GraphSignalArray:
    """Graph signal array

    An array of graph signals."""

    def __init__(self, values, graph=None, copy_values=True, mutable=None,
                 from_transform=None, gsclass=GraphSignal):

        if isinstance(values, int):
            if graph is None:
                raise GraphBadParametersError(
                    "To make a zeroed array with a specified number of "
                    "signals, you must also specify a graph.")
            else:
                values = np.zeros((values, len(graph)))

        self._values = np.asarray(values, dtype=float)
        if len(self._values.shape) != 2:
            raise ValueError(
                "A GraphSignalArray must be constructed from a 2d array.")
        if copy_values:
            self._values = self._values.copy()

        if graph is not None and len(graph) != self._values.shape[1]:
            raise GraphBadParametersError(
                "`values' must be an NxM array, where M is the number of "
                "nodes in the graph.")
        self._graph = graph

        self._transform = from_transform
        self._transform_is_cached = from_transform is not None

        if mutable is None:
            mutable = True
        self._values.flags['WRITEABLE'] = bool(mutable)

        self._gsclass = gsclass

    def __len__(self):
        return len(self._values)

    def __getitem__(self, i):
        if isinstance(i, tuple):
            return self._values[i]
        return self._gsclass(
            self._values[i], graph=self._graph, copy_values=False,
            mutable=self.is_mutable())

    def __setitem__(self, i, value):
        self._values[i] = value

    def _binary_op(self, other, array_op, transform_op=None):
        cls = type(self)
        extra_args = {}

        try:
            other_graph = other.graph
        except AttributeError:
            other_graph = None

        if self.graph is not None:
            graph = self.graph
            if other_graph is not None and other_graph is not graph:
                raise GraphInconsistencyError(
                    "Binary operations between graph signal arrays should not"
                    " act on signal arrays defined for different graphs.")
        else:
            graph = other_graph

        if (transform_op is not None and
            self._transform_is_cached and
            hasattr(other, '_transform_is_cached') and
            other._transform_is_cached
        ):
            if transform_op == 'same':
                transform_op = array_op
            extra_args['from_transform'] = transform_op(
                other._get_transform()._values, self._get_transform()._values)

        return cls(array_op(self._values, np.asarray(other)), graph=graph,
                   copy_values=False, mutable=self.is_mutable(),
                   **extra_args)

    def __add__(self, other):
        return self._binary_op(other, lambda x, y: x + y, transform_op='same')
    __radd__ = __add__

    def __sub__(self, other):
        return self._binary_op(other, lambda x, y: x - y, transform_op='same')

    def __rsub__(self, other):
        return self._binary_op(other, lambda x, y: y - x, transform_op='same')

    def __mul__(self, other):
        return self._binary_op(other, lambda x, y: x * y)
    __rmul__ = __mul__

    def __div__(self, other):
        return self._binary_op(other, lambda x, y: x / y)

    def __rdiv__(self, other):
        return self._binary_op(other, lambda x, y: y / x)

    def __matmul__(self, other):
        return self._values @ np.asarray(other)

    def __rmatmul__(self, other):
        return np.asarray(other) @ self._values

    def norm(self):
        return np.linalg.norm(self._values, axis=-1)

    def is_mutable(self):
        return self._values.flags['WRITEABLE']

    def freeze(self):
        del self._transform_property
        self._values.flags['WRITEABLE'] = False

    def unfreeze(self):
        self._values.flags['WRITEABLE'] = True

    @property
    def graph(self):
        return self._graph

    @graph.setter
    def graph(self, new_graph):
        if new_graph is not None and len(new_graph) != self.shape[-1]:
            raise ValueError(
                "The number of nodes in the graph must be equal to the number "
                "of signal samples.")
        del self.graph  # call the deleter
        self._graph = new_graph

    @graph.deleter
    def graph(self):
        self._del_transform()
        self._graph = None

    @property
    def values(self):
        return self._values

    def max(self, **kwargs):
        return self.values.max(**kwargs)

    def min(self, **kwargs):
        return self.values.min(**kwargs)

    def _get_transform(self):
        if self._transform_is_cached:
            assert not self.is_mutable()
            return self._transform
        if self._graph is None:
            raise ValueError("You can only calculate the graph Fourier "
                             "transform of a signal whose graph is known.")
        transform = self._calc_transform()
        if not self.is_mutable():
            self._set_transform(transform)
        return transform

    def _del_transform(self):
        if self._transform_is_cached:
            self._transform_is_cached = False
            del self._transform

    def _calc_transform(self):
        return (self._graph.get_gft_matrix() @ self._values.T).T

    def _set_transform(self, transform):
        self._transform_is_cached = True
        self._transform = transform

    @property
    def _transform_property(self):
        return self._get_transform()

    @_transform_property.deleter
    def _transform_property(self):
        self._del_transform()

    gft = _transform_property
