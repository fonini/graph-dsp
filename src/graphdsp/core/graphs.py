#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


"""
Graphs module for the Graph Signal Processing package

This module implements classes for representing and operating on graphs.
"""


import itertools

import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt

from .exceptions import GraphBadParametersError
from .signals import GraphSignal, GraphSignalSpectrum


# TODO:
#
# WeightedGraph (que pode ser "directed")
#
# Graph:
# - try to make Graph make as few copies as possible (e.g., __init__ should
#   aim to make zero copies when copy_matrix is false, and only one when it
#   is true). In particular, the line "W=(W+W.T)/2" copies.
#
# GUWG: make the ploting, when plotting grayscale, sort by weight (to avoid
#       white edges above black edges). There ought to be a parameter to make
#       the sorting reversed.
#
# Graph:
# - make a method which returns a signal telling the degree of each vertex. It
#   should accept an optional parameter specifying whether we actually want the
#   *roots* of the degrees.
#
# Signal (graph-domain-only):
# - method .integral() which returns the integral of the signal with respect
#   to the vertex degrees, i.e., f.integral() := sum(f(v)*dv for v in G),
#   where dv is the degree of vertex v.
# - Also, a .volmean() method which returns the volumetric mean, i.e.,
#   the integral divided by the sum of all the degrees in the graph:
#   f.volmean() := f.integral() / sum(dv for v in G)
#
# Graph:
# - factory for special graphs (empty, complete, cycle, circulant)


_machine_epsilon = np.finfo(float).eps  # float64 (double-precision)


class UndirectedWeightedGraph:
    """Undirected Weighted Graph

    Implements a weighted graph with a symmetric Laplacian matrix, as
    described in "The Emerging Field of Signal Processing on Graphs" (SHUMAN
    et al, available in:
        - https://arxiv.org/abs/1211.0053 ,
        - http://ieeexplore.ieee.org/document/6494675 ,
        - https://doi.org/10.1109/MSP.2012.2235192
    ).

    In this package, and specially in this class, graph nodes will also be
    called vertices or points. We will also refer to the graph edges as links.

    Signature:
    UndirectedWeightedGraph(*,
        W=None, L=None, # exactly one must be provided
        nodes=None,
        check_diagonal=True,
        enforce_diagonal=True,
        check_symmetric=True,
        enforce_symmetric=True,
        check_weight_signal=True,
        numerical_error_allowed=<machine epsilon>,
        copy_matrix=True)

    Warning: if you intend to use the parameters check_* and enforce_*, you
    should read the source to understand the exact behaviour that will take
    place.

    Most of the check_* parameters work on an up-to-rounding-errors basis. For
    example, the `check_symmetric` parameter specifies whether the constructor
    shall check, up to rounding errors, that the provided matrix is symmetric.
    This will not be mentioned in each and every one of the parameter
    documentations below.

    keyword-only parameters:
    W, L: The graph is built from the adjacency matrix (aka weights matrix),
        or the Laplacian matrix. Either one, but not both, must be provided.
        You should provide the adjacency matrix through W, or the laplacian
        through L.
    nodes: A list of the nodes of the graph. Useful to store information about
        the nodes in the case that each node represents an object in some
        model; for example, it is used to store the coordinates of the points
        when each graph node is a point in Euclidean space.
    check_diagonal: The default is to check that the diagonal of the provided
        matrix is correct (and raise an exception if it's not). When W is
        provided, its diagonal should be zero. When L is provided, each
        element of its diagonal should be chosen so that all row-sums are
        zero. If this parameter is false, no check is performed.
    enforce_diagonal: When L is provided, the default is to replace its
        diagonal values (which might be near-correct) by the correct ones. If,
        however, this parameter is false, no replacement will occur. When W is
        provided, this parameter has no effect, because of the way that L is
        calculated from W.
    check_symmetric: The default is to check that the given matrix is
        symmetric (and raise an exception if it's not). If this parameter is
        false, no check is performed.
    enforce_symmetric: The default is to replace the given matrix by its
        symmetric part (the symmetric part of a matrix X is (X+X.T)/2). If
        this parameter is false, the matrix is used as is.
    check_weight_signal: The default is to check that the signals of each
        entry in the provided matrix are correct (and raise an exception if
        any are not). If this parameter is false, no check is made.
    numerical_error_allowed: The threshold considered when asserting that the
        checks mentioned above are correct.
    copy_matrix: The default is to copy the provided matrix before
        internalizing and/or making changes. If this parameter is false,
        no copying takes place. There is no guarantee, however, that the
        internalized matrix object is the same as, or shares the same memory
        as, the provided matrix object, even when copy_matrix is false.
    precalc_spectrum: The default is to delay the calculation of the spectral
        decomposition of the graph until it is needed. The first time that the
        eigenvalues or eigenvectors of the graph are needed, they will be
        calculated and cached. If, however, this parameter is true, this
        calculation will take place during the initialization of the graph,
        and then the decomposition will be cached and promptly available for
        whenever it is needed. It is possible to delete the cached spectrum.
        See the 'spectrum' property.
    """

    def __init__(self, *,
                 W=None, L=None,  # exactly one must be specified
                 nodes=None,
                 lmax=None,
                 check_diagonal=True,
                 enforce_diagonal=True,
                 check_symmetric=True,
                 enforce_symmetric=True,
                 check_weight_signal=True,
                 numerical_error_allowed=100*_machine_epsilon,
                 copy_matrix=True,
                 precalc_spectrum=False):

        if L is None:
            if W is None:
                raise GraphBadParametersError(
                    "You must provide either W or L to construct a UWG, "
                    "but you provided neither W nor L.")
            # asarray().copy() might copy twice (i.e., this could be
            # optimized, but it's not exactly a big deal)
            W = np.asarray(W, dtype=float)
            if copy_matrix:
                W = W.copy()
            self._init_from_W(W=W,
                              check_diagonal=check_diagonal,
                              check_symmetric=check_symmetric,
                              enforce_symmetric=enforce_symmetric,
                              check_weight_signal=check_weight_signal,
                              numerical_error_allowed=numerical_error_allowed,
                              lmax=lmax)
        else:
            if W is not None:
                raise GraphBadParametersError(
                    "You must provide either W or L to construct a UWG, "
                    "but you provided both W and L.")
            L = np.asarray(L, dtype=float)
            if copy_matrix:
                L = L.copy()
            self._init_from_L(L=L,
                              check_diagonal=check_diagonal,
                              enforce_diagonal=enforce_diagonal,
                              check_symmetric=check_symmetric,
                              enforce_symmetric=enforce_symmetric,
                              check_weight_signal=check_weight_signal,
                              numerical_error_allowed=numerical_error_allowed,
                              lmax=lmax)
        self._L.flags['WRITEABLE'] = False

        if nodes is None:
            nodes = range(len(self))
        if len(self) != len(nodes):
            raise GraphBadParametersError(
                "The length of the list 'nodes' should be the number of "
                "nodes in the graph. However, the list has length {} but the "
                "graph has {} nodes.".format(len(nodes), len(self)))
        self._nodes = nodes

        self._spectrum_cached = False
        if precalc_spectrum:
            self._calc_spectral_decomposition()

        self._W_is_persistent = False

    @property
    def W(self):
        """Adjacency (weights) matrix.

        When i != j, the entry W[i,j] is the weight of the edge between
        nodes i and j. Since this is an undirected graph, W[i,j] is always the
        same as W[j,i]. High weight values represent a string link between the
        nodes, and low values represent a weaker link. A weight of zero
        represents the absence of an edge. No entries in W can be negative.
        The diagonal entries W[i,i] are always zero.

        This matrix is not internalized in a graph object, and instead is
        calculated every time the property W is accessed. (This is true even
        when the graph was initialized from its adjacency matrix, i.e., using
        the UWG(W=W) syntax.) To cache the value of W and prevent it from
        being calculated every time, use the method make_W_persistent.
        To dismiss the cached value, use the method delete_cached_W, or just
        delete the W property (del graph.W).

        This property is also available as graph.adjacency instead of graph.W.
        """
        if self._W_is_persistent:
            return self._W
        # The docs are not clear about whether "-L" copies
        _W = -self._L.copy()
        np.fill_diagonal(_W, 0)
        return _W

    def make_W_persistent(self):
        """Internalize the adjacency matrix."""
        if not self._W_is_persistent:
            assert not hasattr(self, '_W')
            self._W = self.W
            self._W.flags['WRITEABLE'] = False
            self._W_is_persistent = True
        else:
            assert hasattr(self, '_W')
        return self.W

    def delete_cached_W(self):
        """Dismiss the cached adjacency matrix.

        Running `graph.delete_cached_W()` is equivalent to `del graph.W`.
        """
        if self._W_is_persistent:
            assert hasattr(self, '_W')
            self._W_is_persistent = False
            del self._W
        else:
            assert not hasattr(self, '_W')

    @W.deleter
    def W(self):
        self.delete_cached_W()

    @property
    def L(self):
        """Laplacian matrix.

        When i != j, the entry L[i,j] is equal to -W[i,j]. When L[i,i] is a
        diagonal entry of the Laplacian, its value is the sum of the weights
        of all the edges that incide on node i (so that all row sums of the
        Laplacian are zero).

        This property is also available as graph.laplacian instead of graph.L.
        """
        return self._L

    def __len__(self):
        """Returns the number of nodes in the graph."""
        return self.L.shape[0]

    @property
    def nodes(self):
        """Iterable of nodes in the graph."""
        return self._nodes

    @property
    def spectrum(self):
        """The spectrum of the graph.

        This is the list of eigenvalues of the graph Laplacian (see the 'L'
        property), in increasing order.

        This property, and all other properties and functions that depend on
        the spectral decomposition of the graph, uses a cached decomposition,
        and only calculates the decomposition if no cached version is
        available. To delete the cached spectral decomposition, use the method
        'delete_cached_spectral_decomposition', or just run
        'del graph.spectrum'.
        """
        if not self._spectrum_cached:
            self._calc_spectral_decomposition()
        return self._spectrum

    # TODO: property "eigenvectors" that returns an object of a specialized
    #       class for holding eigenvectors of a graph. An instance of this
    #       class should behave like a list of GraphSignal. (That is, it
    #       should implement __getitem__, the iterator protocol, etc.) This
    #       "eigenvectors" property should, of course, cache its result. This
    #       cached result must be deleted when the graph spectrum cache is
    #       deleted---but the "eigenvectors" should not be calculated upon
    #       creation of the graph spectrum cache (i.e., the "eigenvectors"
    #       property should only be calculated when requested explicitly)

    @property
    def U(self):
        """Graph eigenvector matrix.

        The matrix whose i-th column is the i-th graph eigenvectors.
        """
        if not self._spectrum_cached:
            self._calc_spectral_decomposition()
        return self._U

    def diag_lambda(self):
        """Diagonal matrix with graph eigenvalues."""
        if not self._spectrum_cached:
            self._calc_spectral_decomposition()
        return np.diag(self._spectrum)

    def spectral_decomposition(self):
        """A tuple with the graph spectrum and the eigenvectors matrix.

        This is a convenience function that returns a tuple with the graph
        spectrum and the eigenvectors matrix. The graph spectrum is a vector
        with the eigenvalues (in increasing order), and the eigenvector
        matrix is the matrix whose columns are the corresponding eigenvectors.
        """
        if not self._spectrum_cached:
            self._calc_spectral_decomposition()
        return self._spectrum, self._U

    def delete_cached_spectral_decomposition(self):
        """Dismiss the cached graph spectrum and eigenvector matrix."""
        if self._spectrum_cached:
            self._spectrum_cached = False
            del self._spectrum
            del self._U

    @spectrum.deleter
    def spectrum(self):
        self.delete_cached_spectral_decomposition()

    def get_gft_matrix(self):
        if not self._spectrum_cached:
            self._calc_spectral_decomposition()
        return self._U.T

    def gft(self, x):
        """Graph Fourier transform of a GraphSignal"""
        return self._apply_transform(x, GraphSignal,
                                     self.get_gft_matrix())

    def get_igft_matrix(self):
        if not self._spectrum_cached:
            self._calc_spectral_decomposition()
        return self._U

    def igft(self, x):
        """Inverse graph Fourier transform of a GraphSignalSpectrum"""
        return self._apply_transform(x, GraphSignalSpectrum,
                                     self.get_igft_matrix())

    @property
    def edges(self):
        _W = self.W
        return [(i, j)
                for i, j in itertools.product(range(len(self)), repeat=2)
                if j > i and _W[i, j] > 0]

    def visualize_laplacian(self):
        """Plots the Laplacian matrix L."""
        plt.figure()
        plt.matshow(np.abs(self.L))
        plt.xticks([])
        plt.yticks([])
        plt.colorbar()

    def visualize_adjacency(self):
        """Plots the adjacency matrix W."""
        plt.figure()
        plt.matshow(np.abs(self.W))
        plt.xticks([])
        plt.yticks([])
        plt.colorbar()

    # aliases
    adjacency = W
    laplacian = L
    ev_matrix = U

    def _apply_transform(self, x, expected_class, matrix):
        if not isinstance(x, expected_class):
            x = expected_class(x, graph=self)
        if x._transform_is_cached:
            return x._transform
        return x._transform_class(
            (matrix @ x._values.T).T,
            graph=self,
            copy_values=False,
            from_transform=x)

    def _init_from_W(self, W, check_diagonal, check_symmetric,
                     enforce_symmetric, check_weight_signal,
                     numerical_error_allowed, lmax):
        """Initializes the Laplacian from a user-provided adjacency matrix.

        This method considers that the parameter W can be written to. If this
        behaviour is not desired, the caller should copy the matrix before
        handing it over to this method.
        """

        self._check_square_matrix(W, "W")

        if check_diagonal:
            diag_norm = np.linalg.norm(W.diagonal())
            if diag_norm > len(W) * numerical_error_allowed:
                raise GraphBadParametersError(
                    "W should have an empty diagonal. "
                    "Instead, diagonal has norm {}".format(diag_norm))
            # self._checked diagonal indicates whether we checked, during
            # __init__, that the given W's diagonal was zero.
            self._checked_diagonal = True
        else:
            self._checked_diagonal = False

        # When W is provided, L's diagonal will always be correct.
        # self._enforced_diagonal indicates that we forced L's diagonal to
        # be minus the sum of the other entries in each row (that is, we
        # forced L's rows to add up to zero by tweaking its diagonal).
        self._enforced_diagonal = True

        if check_symmetric:
            self._check_symmetric(W, "W", numerical_error_allowed)
            # self._checked_symmetric indicates whether we checked,
            # during __init__, that the given W was symmetric.
            self._checked_symmetric = True
        else:
            self._checked_symmetric = False

        if enforce_symmetric:
            W = (W + W.T) / 2
            # self._enforced_symmetric indicates that we forced W (and
            # therefore L) to be symmetric.
            self._enforced_symmetric = True
        else:
            self._enforced_symmetric = False

        if check_weight_signal:
            if (W < 0).any():
                raise GraphBadParametersError(
                    "W should not have any negative entries, but the "
                    "provided W does.")
            # self._checked_weight_signals indicates whether we checked,
            # during construction of the graph, that the provided W had all
            # entries non-negative.
            self._checked_weight_signals = True
        else:
            self._checked_weight_signals = False

        np.fill_diagonal(W, 0)
        self._L = -W
        np.fill_diagonal(self._L, W.sum(axis=1))
        if lmax is not None:
            self._lmax_factor = lmax / linalg.eigh(self._L)[0].max()
            self._L *= self._lmax_factor
        else:
            self._lmax_factor = 1

        self._initialized_from = "W"

    def _init_from_L(self, L, check_diagonal, enforce_diagonal,
                     check_symmetric, enforce_symmetric,
                     check_weight_signal, numerical_error_allowed):
        """Initializes the Laplacian from the user-provided Laplacian matrix.

        This method considers that the parameter L can be written to. If this
        behaviour is not desired, the caller should copy the matrix before
        handing it over to this method.
        """

        self._check_square_matrix(L, "L")

        # see the comments in _init_from_W for the meanings of
        # self._enforced_* and self._checked_* .

        if check_diagonal:
            self._check_L_diagonal(L, numerical_error_allowed)
            self._checked_diagonal = True
        else:
            self._checked_diagonal = False

        if enforce_diagonal:
            np.fill_diagonal(L, 0)
            np.fill_diagonal(L, -L.sum(axis=1))
            self._enforced_diagonal = True
        else:
            self._enforced_diagonal = False

        if check_symmetric:
            self._check_symmetric(L, "L", numerical_error_allowed)
            self._checked_symmetric = True
        else:
            self._checked_symmetric = False

        if enforce_symmetric:
            L = (L + L.T) / 2
            self._enforced_symmetric = True
        else:
            self._enforced_symmetric = False

        if check_weight_signal:
            L_positive = L * (2*np.eye(len(L)) - 1)
            if (L_positive < 0).any():
                raise GraphBadParametersError(
                    "L should be non-negative on the diagonal and "
                    "non-positive everywhere else, but the provided L does "
                    "not meet this restriction.")
            self._checked_weight_signals = True
        else:
            self._checked_weight_signals = False

        self._L = L
        if lmax is not None:
            self._lmax_factor = lmax / linalg.eigh(self._L)[0].max()
            self._L *= self._lmax_factor
        else:
            self._lmax_factor = 1

        self._initialized_from = "L"

    def _calc_spectral_decomposition(self):
        spectrum, U = linalg.eigh(self._L)
        sort_idx = spectrum.argsort()
        self._spectrum = spectrum[sort_idx].copy()
        self._spectrum[0] = 0
        self._spectrum.flags['WRITEABLE'] = False
        self._U = U[:, sort_idx].copy()
        self._U[:, 0] = 1/np.sqrt(len(self))
        self._U.flags['WRITEABLE'] = False
        self._spectrum_cached = True

    @staticmethod
    def _check_square_matrix(matrix, name, error=GraphBadParametersError):
        if len(matrix.shape) != 2:
            raise error(
                "{} must be a bi-dimensional array (i.e., a matrix). "
                "Instead, {} has shape {}.".format(name, name, matrix.shape))
        if matrix.shape[0] != matrix.shape[1]:
            raise error(
                "{} must be a square matrix. "
                "Instead, {} has shape {}.".format(name, name, matrix.shape))

    @staticmethod
    def _check_symmetric(array, name, numerical_error_allowed,
                         error=GraphBadParametersError):
        antisym = (array - array.T) / 2
        antisym_norm = np.linalg.norm(antisym, 2)
        if antisym_norm > numerical_error_allowed:
            raise error(
                "{} should be symmetric. The antisymmetric part of "
                "{} has norm {}".format(name, name, antisym_norm))

    @staticmethod
    def _check_L_diagonal(L, numerical_error_allowed,
                          error=GraphBadParametersError):
        residual_norm = np.linalg.norm(L.sum(axis=1))
        if residual_norm > len(L) * numerical_error_allowed:
            raise GraphBadParametersError(
                "L's diagonal should be set so that all of its row-sums are "
                "zero. Instead, vector of row-sums has "
                "norm {}".format(residual_norm))


class GeometricUWG(UndirectedWeightedGraph):
    """Geometric undirected weighted graph

    Implements a UWG in which each node is point in Euclidean space.

    Signature:
    GeometricUWG(
        points, *,
        populate_edges=None, W=None, L=None, # specify exactly one
        copy_matrix=True,
        check_diagonal=True,
        numerical_error_allowed=<machine_epsilon>,
        **uwg_args)

    positional parameters:
    points: Iterable of points (2d array, or list of 1d arrays) in Euclidean
        space. Each such point will be a node in the graph.

    keyword parameters:
    populate_edges: Callable that constructs an adjacency matrix from a list
        of points (usually taking into consideration the distance between each
        pair of points). This callable receives one argument (the 'points'
        parameter) and returns W, the adjacency matrix. This argument is not
        mandatory; instead, you might prefer to provide directly the adjacency
        matrix W, or the Laplacian matrix L. See the 'W' and 'L' keyword
        parameters.
    W: The adjacency matrix of the graph. This argument is not mandatory;
        instead, you might prefer to provide the laplacian matrix L, or
        alternatively provide a function that will generate the adjacency
        matrix given the list of points. See the 'L' and 'populate_edges'
        keyword parameters.
    L:  The Laplacian matrix of the graph. This argument is not mandatory;
        instead, you might prefer to provide the adjacency matrix W, or
        alternatively provide a function that will generate the adjacency
        matrix given the list of points. See the 'W' and 'populate_edges'
        keyword parameters.
    copy_matrix: The default is to copy the provided matrix (or the matrix
        returned from populate_edges) before
        internalizing and/or making changes. If this parameter is false,
        no copying takes place. There is no guarantee, however, that the
        internalized matrix object is the same as, or uses the same memory as,
        the provided matrix object, even when copy_matrix is false.
    check_diagonal: The default is to check that the diagonal of the provided
        matrix (or of the matrix returned from populate_edges) is correct (and
        raise an exception if it's not). When W is
        provided, its diagonal should be zero. When L is provided, each
        element of its diagonal should be chosen so that all row-sums are
        zero. If this parameter is false, no check is performed.
    numerical_error_allowed: The threshold for numerical correctness
        considered when asserting that lots of Graph preconditions are
        satisfied (see UndirectedWeightedGraph), and when checking whether two
        edges intersect.
    **uwg_args: passed directly to the constructor of UndirectedWeightedGraph.
    """

    def __init__(self, points, *,
                 populate_edges=None, W=None, L=None,  # specify exactly one
                 copy_matrix=True,
                 check_diagonal=True,
                 numerical_error_allowed=100*_machine_epsilon,
                 **uwg_args):
        if populate_edges is not None:
            if W is not None or L is not None:
                raise GraphBadParametersError(
                    "GeometricUWG: you should provide either the "
                    "populate_edges factory or directly one of the matrices "
                    "L or W. However, you provided two or more of the three "
                    "initialization methods.")
            # else: the provided populate_edges will be used
        else:
            if W is None:
                if L is None:
                    raise GraphBadParametersError(
                        "GeometricUWG: you should provide either the "
                        "populate_edges factory or directly one of the "
                        "matrices L or W. However, you provided none of the "
                        "three initialization methods.")
                else:
                    if check_diagonal:
                        self._check_L_diagonal(L, numerical_error_allowed)
                    L_self = L.copy() if copy_matrix else L
                    copy_matrix = False  # no need to copy twice
                    def _populate_edges_from_L(points, _L=L_self):
                        W = _L
                        np.fill_diagonal(W, 0)
                        W *= -1
                        return W
                    populate_edges = _populate_edges_from_L
            else:
                if L is not None:
                    raise GraphBadParametersError(
                        "GeometricUWG: when populate_edges is not provided, "
                        "you must specify at most one of the matrices W or "
                        "L, but you specified both.")
                else:
                    W_self = W.copy() if copy_matrix else W
                    copy_matrix = False  # no need to copy twice
                    def _populate_edges_from_W(points, _W=W_self):
                        return _W
                    populate_edges = _populate_edges_from_W
        # copying of W, if needed, will take place inside super().__init__
        W = populate_edges(points)
        if len(W) != len(points):
            raise GraphBadParametersError(
                "The list of points (nodes) in the geometric graph should be "
                "the same length as the order of the square matrix W.")
        super().__init__(W=W,
                         nodes=points,
                         copy_matrix=copy_matrix,
                         check_diagonal=check_diagonal,
                         numerical_error_allowed=numerical_error_allowed,
                         **uwg_args)

    # TODO: adicionar uma opção 'isolated' nas funções de populate.
    # populate(..., isolated=None)     ->  implementa do jeito que tá
    # populate(..., isolated='redo')   ->  se, ao final de tudo, houver
    #     pontos isolados, refaz tudo.
    # populate(..., isolated='force_edge')  ->  se, ao final de tudo,
    #     houver pontos isolados, adiciona uma aresta desse ponto ao
    #     vértice mais próximo
    @classmethod
    def make_random(cls, *,
                    N=None, points=None,  # one must be specified
                    points_geothresh=0.0,
                    Wthresh=0.0,
                    W_geothresh=float('inf'),
                    d=2,
                    random_point_factory=None,
                    kernel=None,
                    planar=False,
                    numerical_error_allowed=100*_machine_epsilon,
                    lmax=None):
        """Factory method that returns a random graph.

        This factory makes two things:

        1. Generates N random points. These points will be the nodes of the
            graph.
        2. Connect the points through edges, according to a rule that takes
            into consideration the distances between points.

        keyword-only parameters:
        N: The number of points (nodes) in the graph. This parameter is not
            mandatory; instead, you might prefer to provide directly the list
            of points (see the 'points' parameter). If you provide the 'N'
            parameter, N points will be randomly generated (see also the
            'random_point_factory' parameter).
        points: The list of points (see the documentation for the constructor
            of this class). This parameter is not mandatory; instead, you
            might prefer to provide just the number of points, and let the
            determination of the location of points be chosen at random. See
            the 'N' parameter.
        points_geothresh: Geometric threshold for placement of nearby points.
            The random point generation guarantees that no two points will be
            closer than 'points_geothresh'. By default, this threshold is
            zero, that is, there is no restriction on how close two points
            can be.
        Wthresh: By default, all pairs of points are connected by edges whose
            weights are the result of the kernel function (see the 'kernel'
            parameter) applied to the distance between the points (i.e., the
            length of the edge). If Wthresh is supplied, it acts as a
            threshold for the weight values: any edge weight below Wthresh
            will be truncated to zero. The result is that the adjacency matrix
            will be sparsified by zeroing out all entries that were already
            sufficiently small.
        W_geothresh: This parameter has the same purpose as Wthresh, that is,
            sparsifying the adjacency matrix. The only difference is that with
            this parameter, the threshold is specified as a distance between
            the points: all edges whose length is above 'W_geothresh' will be
            deleted. If the kernel function is a decreasing function of the
            edge length (it should usually be), the two parameters are
            interchangeable, that is, W_geothresh=D is the same as
            Wthresh=kernel(D).
        d: the dimensionality of the the Euclidean space on which the graph
            nodes live. The plotting functionalities are implemented only
            for d=2.
        random_point_factory: this is a callable that takes zero arguments and
            returns a random point (that is, a numpy 1d-array with length 'd'
            and random entries). The default is a simple function that returns
            a point chosen from the uniform distribution on the unit cube of
            the Euclidean space with appropriate dimension.
        kernel: this is a callable that receives the euclidean distance
            between two points and returns the edge weight between them. There
            is no need to implement saturation to zero here; sparsification of
            the adjacency matrix can be accomplished with the Wthresh or
            W_geothresh parameters. See the factories 'make_invsquare_kernel'
            and 'make_gauss_kernel'.
        planar: By default, edges are allowed to intersect. If this parameter
            is true, the returned graph is guaranteed to be planar (that is,
            no edges will intersect). Planar graphs must be two-dimensional
            (i.e., d=2). The algorithm is: delete any edge that is not
            smaller than all of the edges it intersects, and repeat until
            there are no more intersections.
        numerical_error_allowed: The threshold for accepted numerical
            errors. This is used in making sure that edges with a common node
            are not interpreted as intersecting each other, and also
            other things mentioned in the constructor of
            UndirectedWeightedGraph.
        """

        if random_point_factory is None:
            def random_point_factory():
                return np.random.random_sample((d,))

        if kernel is None:
            kernel = cls.make_invsquare_kernel()

        if N is None:
            if points is None:
                raise GraphBadParametersError(
                    "You must specify either N or points, but you provided "
                    "none.")
            # Here, points was provided
            if d == 2 and len(points) > 0:
                d = len(points[0])
        else:
            if points is not None:
                raise GraphBadParametersError(
                    "You must provide exactly one of 'N' or points, but you "
                    "provided both.")
            points = cls._make_random_points(
                N, d,
                point_factory=random_point_factory,
                geothresh=points_geothresh)

        if planar and d != 2:
            raise GraphBadParametersError("A planar graph must have d=2.")

        def populate_edges(points):
            return cls._populate_edges(
                points,
                kernel=kernel,
                Wthresh=Wthresh,
                geothresh=W_geothresh,
                planar=planar,
                numerical_error_allowed=numerical_error_allowed)

        return cls(points,
                   populate_edges=populate_edges,
                   copy_matrix=False,
                   numerical_error_allowed=numerical_error_allowed,
                   lmax=lmax)

    @classmethod
    def make_random_pretty(cls, preset='default', *,
                           N=50,
                           Wthresh=20,
                           points_geothresh=0.12,
                           W_geothresh=0.28,
                           random_point_factory=None,
                           kernel=None,
                           planar=False,
                           numerical_error_allowed=100*_machine_epsilon,
                           lmax=1):
        """Factory method for generating a random graph that looks nice.

        This is mostly the same as the make_random method, except that
        you will end up with a nice-looking graph if you stay with the
        defaults. This method only returns 2d graphs.

        Sinopsis:
        GeometricUWG.make_random_pretty() -> Returns a random, nice-looking
            graph.
        GeometricUWG.make_random_pretty('default') -> The same as above.
        GeometricUWG.make_random_pretty('planar') -> The same, but makes sure
            that there are no intersections between the edges.
        GeometricUWG.make_random_pretty('planar2') -> The same as above, but
            uses the point-placement parameters used in 'default'.
        """
        if random_point_factory is None:
            def random_point_factory():
                return np.sqrt(N/50) * np.random.random_sample((2,))
        if preset in {None, '', 'default'}:
            return cls.make_random(
                N=N,
                Wthresh=Wthresh,
                points_geothresh=points_geothresh,
                # do not set W_geothresh
                random_point_factory=random_point_factory,
                kernel=kernel,
                planar=planar,
                numerical_error_allowed=numerical_error_allowed,
                lmax=lmax)
        if preset == 'planar':
            return cls.make_random(
                N=N,
                # do not set Wthresh
                points_geothresh=points_geothresh,
                W_geothresh=W_geothresh,
                random_point_factory=random_point_factory,
                kernel=kernel,
                planar=True,
                numerical_error_allowed=numerical_error_allowed,
                lmax=lmax)
        if preset == 'planar2':
            return cls.make_random(
                N=N,
                Wthresh=Wthresh,
                points_geothresh=points_geothresh,
                # do not set W_geothresh
                random_point_factory=random_point_factory,
                kernel=kernel,
                planar=True,
                numerical_error_allowed=numerical_error_allowed,
                lmax=lmax)
        raise GraphBadParametersError("Unknown preset `{}'.".format(preset))

    @staticmethod
    def make_invsquare_kernel(scale=1):
        """Inverse square kernel for distance-based adjacency matrices.

        Given 'scale', returns a callable that receives 'x' and returns the
        inverse square of 'x/scale'.

        Usage:

            kernel = GeometricUWG.make_invsquare_kernel(100)
            graph = GeometricUWG.make_random(......, kernel=kernel)
        """
        def invsquare_kernel(x, _scale2=(scale * scale)):
            return _scale2 / (x * x)
        return invsquare_kernel

    @staticmethod
    def make_gauss_kernel(sigma):
        """Gaussian kernel for distance-based adjacency matrices.

        Given 'sigma', returns a callable that receives 'x' and returns
        exp{ - 0.5 x^2 / sigma^2 }.

        Usage:

            kernel = GeometricUWG.make_gauss_kernel(100)
            graph = GeometricUWG.make_random(......, kernel=kernel)
        """
        def gauss_kernel(x, _lambd=(0.5 / (sigma * sigma))):
            return np.exp(-_lambd * x * x)
        return gauss_kernel

    @classmethod
    def _populate_edges(cls, points, *, kernel, Wthresh, geothresh,
                        numerical_error_allowed, planar=False):
        W = cls._apply_kernel(points, kernel)
        W[W < Wthresh] = 0
        N = len(points)
        edges = set()
        for i, j in itertools.product(range(N), repeat=2):
            if j <= i:
                continue
            # Make a new edge (i,j)
            new_edge = (i, j)
            new_edge_length = np.linalg.norm(
                points[new_edge[1]] - points[new_edge[0]])
            # First thing to do: check if new_edge meets the
            # Wthresh and geothresh criterions:
            if W[i, j] == 0.0 or new_edge_length > geothresh:
                # Criterions were not met.
                # new_edge will not be added to the graph:
                W[new_edge[0], new_edge[1]] = 0
                W[new_edge[1], new_edge[0]] = 0
                continue
            if not planar:
                continue
            # Check if the new edge intersects any other
            # existing edge. If it does, check if it is smaller (in terms of
            # length) than all of them. If it is, delete them, and the new
            # edge will be added to the graph.
            # Start by listing the edges that new_edge intersects:
            intersections = [e for e in edges if cls._edges_intersect(
                # TODO: this is quite slow.
                # * Optimization option #1: write this as a proper loop, and
                #   check the length at each iteration. As soon as we find a
                #   smaller intersected edge, we can stop early.
                # * Optimization option #2: rewrite 'edges_intersect' in C.
                # * Optimization option #3: do both of the above :)
                (points[e[0]], points[e[1]]),
                (points[new_edge[0]], points[new_edge[1]]),
                thresh=numerical_error_allowed
            )]
            # Check if it's the smallest:
            for e in intersections:
                e_norm = np.linalg.norm(points[e[1]] - points[e[0]])
                if e_norm <= new_edge_length:
                    # Test edge "e" is smaller than new_edge.
                    # new_edge will not be added to the graph:
                    W[new_edge[0], new_edge[1]] = 0
                    W[new_edge[1], new_edge[0]] = 0
                    break
            else:
                # If we're here, new_edge is smaller than
                # all edges it intersects.
                # Remove the intersected edges:
                for e in intersections:
                    edges.remove(e)
                    W[e[0], e[1]] = 0
                    W[e[1], e[0]] = 0
                # ...and add new_edge to the graph:
                edges.add(new_edge)
        return W

    @staticmethod
    def _apply_kernel(points, kernel):
        # TODO: accept a "kernel2" parameter, which will be an alternative
        #       to 'kernel', and will receive the raw pair of points, instead
        #       of receiving the calculated distance.
        N = len(points)
        W = np.zeros((N, N))
        for i, j in itertools.product(range(N), repeat=2):
            if j <= i:
                continue
            weight = kernel(np.linalg.norm(points[j] - points[i]))
            W[i, j] = weight
            W[j, i] = weight
        return W

    @staticmethod
    def _make_random_points(N, d, *, point_factory, geothresh):
        points = np.empty((N, d))
        for i in range(N):
            while True:
                p = point_factory()
                for j in range(i):
                    dist = np.sqrt(((p - points[j])**2).sum())
                    if dist < geothresh:
                        break
                else:
                    break
            points[i] = p
        return points

    @staticmethod
    def _edges_intersect(e, f, *, thresh):
        """Check if edges intersect

        'e' and 'f' are the edges,
        represented by pairs (tuples) of arrays (endpoints)
        'thresh' is a threshold for numerical errors. By default,
        equal to the machine epsilon.
        """
        p = e[0]
        q = f[0]
        r = e[1] - e[0]
        s = f[1] - f[0]
        cross = np.cross(r, s).astype(float)
        if np.abs(cross) < thresh:
            # edges are parallel
            if np.cross(q - p, r) < thresh:
                # edges are collinear
                if np.linalg.norm(r) == 0:
                    return False
                t0 = (q - p).dot(r) / r.dot(r)
                t1 = t0 + s.dot(r) / r.dot(r)
                if t1 < t0:
                    t0, t1 = t1, t0
                if t0 < 1-thresh and t1 > 0+thresh:
                    return True
                else:
                    return False
            else:
                # edges are properly parallel
                return False
        else:
            # r x s != 0 ==> edges are not parallel at all
            t = np.cross(q - p, s) / np.cross(r, s)
            u = np.cross(q - p, r) / np.cross(r, s)
            if (0+thresh < t < 1-thresh) and (0+thresh < u < 1-thresh):
                return True
            else:
                return False
