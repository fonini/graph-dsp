#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


"""
Exceptions module for the Graph Signal Processing package

All exceptions derive from the base GSPError, which in turn derives from
the builtin Exception.
"""


class GSPError(Exception):
    """Graph Signal Processig Error

    Base class for the exceptions raised by the graphdsp package.
    """


class GraphBadParametersError(GSPError, ValueError):
    """Exception raised by UWG with instantiated with invalid parameters."""


class GraphInconsistencyError(GSPError, ValueError):
    """Exception raised when signals for different graphs interact."""
