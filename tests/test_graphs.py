#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


"""
Test module for undirected weighted graphs
"""


import pytest

import numpy as np

import graphdsp as gsp


class TestUWG:

    def test_init_W(self):

        with pytest.raises(TypeError, match='2 were given'):
            gsp.UndirectedWeightedGraph([[0, 1], [1, 0]])

        with pytest.raises(gsp.GraphBadParametersError,
                           match='neither W nor L'):
            gsp.UndirectedWeightedGraph()

        assert gsp.UndirectedWeightedGraph.adjacency is \
            gsp.UndirectedWeightedGraph.W

        W = [[0, 1, 2, 0],
             [1, 0, 0, 0],
             [2, 0, 0, 3],
             [0, 0, 3, 0]]

        g = gsp.UndirectedWeightedGraph(W=W)
        assert isinstance(g.W, np.ndarray)
        assert isinstance(g.L, np.ndarray)
        assert g._initialized_from == 'W'

        Wa = np.array(W, dtype=float)
        g = gsp.UndirectedWeightedGraph(W=Wa)
        Wa[0, 0] = 1234
        assert g.W[0, 0] == 0

        # nothing can be guaranteed, but it, at least, shouldn't blow up
        gsp.UndirectedWeightedGraph(W=W, copy_matrix=False)

        with pytest.raises(gsp.GraphBadParametersError,
                           match='W must be.*square'):
            gsp.UndirectedWeightedGraph(W=[[0, 1, 1], [2, 3, 5]])

        W_noise = np.asarray(W, dtype=float)
        np.fill_diagonal(W_noise,
                         1e-20 * np.random.random_sample(W_noise.shape))
        g = gsp.UndirectedWeightedGraph(W=W_noise)
        assert g._checked_diagonal
        assert g._enforced_diagonal
        assert (g.W.diagonal() == np.zeros((len(g),))).all()
        assert g.L.sum(axis=1) == pytest.approx(np.zeros((len(g),)))

        W_noise = np.asarray(W, dtype=float)
        np.fill_diagonal(W_noise, 0.1)
        with pytest.raises(gsp.GraphBadParametersError,
                           match='W should have.*empty diagonal'):
            gsp.UndirectedWeightedGraph(W=W_noise)

        W_noise = np.asarray(W, dtype=float)
        np.fill_diagonal(W_noise,
                         1e+20 * np.random.random_sample(W_noise.shape))
        g = gsp.UndirectedWeightedGraph(W=W_noise, check_diagonal=False)
        assert (g.W.diagonal() == np.zeros((len(g),))).all()
        assert g.L.sum(axis=1) == pytest.approx(np.zeros((len(g),)))
        assert not g._checked_diagonal
        assert g._enforced_diagonal

        W_noise = np.asarray(W, dtype=float)
        W_noise += 1e-20 * np.random.random_sample(W_noise.shape)
        g = gsp.UndirectedWeightedGraph(W=W_noise)
        assert g._checked_symmetric
        assert g._enforced_symmetric

        W_noise = np.asarray(W, dtype=float)
        W_noise += 1e+20 * np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        with pytest.raises(gsp.GraphBadParametersError,
                           match='W .*should .*symmetric'):
            gsp.UndirectedWeightedGraph(W=W_noise)

        W_noise = np.asarray(W, dtype=float)
        W_noise += 1e+20 * np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        g = gsp.UndirectedWeightedGraph(W=W_noise, check_symmetric=False)
        assert not g._checked_symmetric
        assert g._enforced_symmetric
        assert (g.W == g.W.T).all()  # equality, not approx equality

        W_noise = np.asarray(W, dtype=float)
        W_noise += 1e+20 * np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        g = gsp.UndirectedWeightedGraph(W=W_noise, check_symmetric=False,
                                        enforce_symmetric=False)
        assert not g._checked_symmetric
        assert not g._enforced_symmetric
        assert (W_noise == g.W).all()

        W_noise = np.asarray(W, dtype=float)
        W_noise += 1e-20 * np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        g = gsp.UndirectedWeightedGraph(W=W_noise, check_symmetric=True,
                                        enforce_symmetric=False)
        assert g._checked_symmetric
        assert not g._enforced_symmetric
        assert (W_noise == g.W).all()

        g = gsp.UndirectedWeightedGraph(W=W)
        assert g._checked_weight_signals

        W_noise = np.asarray(W, dtype=float)
        W_noise[0, 3] = -1e-200
        W_noise[3, 0] = -1e-200
        with pytest.raises(gsp.GraphBadParametersError,
                           match='W should.*n.*negative'):
            gsp.UndirectedWeightedGraph(W=W_noise)

        W_noise = np.asarray(W, dtype=float)
        W_noise[0, 3] = -1
        W_noise[3, 0] = -1
        g = gsp.UndirectedWeightedGraph(W=W_noise, check_weight_signal=False)
        assert not g._checked_weight_signals
        assert g.W[0, 3] < 0

    def test_init_L(self):

        assert gsp.UndirectedWeightedGraph.laplacian is \
            gsp.UndirectedWeightedGraph.L

        W = np.array(
            [[0, 1, 2, 0],
             [1, 0, 0, 0],
             [2, 0, 0, 3],
             [0, 0, 3, 0]], dtype=float)
        L = -W
        np.fill_diagonal(L, W.sum(axis=1))
        diag_mask = np.zeros(L.shape, dtype=bool)
        np.fill_diagonal(diag_mask, True)

        with pytest.raises(gsp.GraphBadParametersError,
                           match='both W and L'):
            gsp.UndirectedWeightedGraph(W=W, L=L)

        g = gsp.UndirectedWeightedGraph(L=L)
        assert isinstance(g.W, np.ndarray)
        assert isinstance(g.L, np.ndarray)
        assert g._initialized_from == 'L'
        assert g._checked_weight_signals

        La = np.array(L, dtype=float)
        g = gsp.UndirectedWeightedGraph(L=La)
        La[0, 0] = 1234
        assert g.L[0, 0] == pytest.approx(L[0, 0])

        # nothing can be guaranteed, but it, at least, shouldn't blow up
        gsp.UndirectedWeightedGraph(L=L, copy_matrix=False)

        with pytest.raises(gsp.GraphBadParametersError,
                           match='L must be.*square'):
            gsp.UndirectedWeightedGraph(L=[[0, 1, 1], [2, 3, 5]])

        L_noise = np.array(L, dtype=float)
        new_diag = L.diagonal() + 1e-20*np.random.random_sample(L_noise.shape)
        np.fill_diagonal(L_noise, new_diag)
        g = gsp.UndirectedWeightedGraph(L=L_noise)
        assert g._checked_diagonal
        assert g._enforced_diagonal
        assert (g.W.diagonal() == np.zeros((len(g),))).all()
        assert g.L.sum(axis=1) == pytest.approx(np.zeros((len(g),)))

        L_noise = np.array(L, dtype=float)
        new_diag = L.diagonal() + 0.1
        np.fill_diagonal(L_noise, new_diag)
        with pytest.raises(gsp.GraphBadParametersError,
                           match='L.*diagonal.*row.*sum.*zero'):
            gsp.UndirectedWeightedGraph(L=L_noise)

        L_noise = np.array(L, dtype=float)
        new_diag = L.diagonal() + 1e-20*np.random.random_sample(L_noise.shape)
        np.fill_diagonal(L_noise, new_diag)
        g = gsp.UndirectedWeightedGraph(L=L_noise, enforce_diagonal=False)
        assert g._checked_diagonal
        assert not g._enforced_diagonal
        assert (g.L == L_noise).all()

        L_noise = np.array(L, dtype=float)
        new_diag = L.diagonal() + 1e+20*np.random.random_sample(L_noise.shape)
        np.fill_diagonal(L_noise, new_diag)
        g = gsp.UndirectedWeightedGraph(L=L_noise, check_diagonal=False)
        assert (g.W.diagonal() == np.zeros((len(g),))).all()
        assert g.L.sum(axis=1) == pytest.approx(np.zeros((len(g),)))
        assert not g._checked_diagonal
        assert g._enforced_diagonal

        L_noise = np.array(L, dtype=float)
        new_diag = L.diagonal() + 1e+20*np.random.random_sample(L_noise.shape)
        np.fill_diagonal(L_noise, new_diag)
        g = gsp.UndirectedWeightedGraph(L=L_noise, check_diagonal=False,
                                        enforce_diagonal=False)
        assert (g.W.diagonal() == np.zeros((len(g),))).all()
        assert (g.L == L_noise).all()
        assert not g._checked_diagonal
        assert not g._enforced_diagonal

        W_noise = np.array(W, dtype=float)
        W_noise += 1e-20 * np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        L_noise = -W_noise
        np.fill_diagonal(L_noise, W_noise.sum(axis=1))
        g = gsp.UndirectedWeightedGraph(L=L_noise)
        assert g._checked_symmetric
        assert g._enforced_symmetric
        assert (g.W == g.W.T).all()
        assert (g.L == g.L.T).all()

        W_noise = np.array(W, dtype=float)
        W_noise += np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        L_noise = -W_noise
        np.fill_diagonal(L_noise, W_noise.sum(axis=1))
        with pytest.raises(gsp.GraphBadParametersError,
                           match='L should be.*symmetric'):
            gsp.UndirectedWeightedGraph(L=L_noise)

        W_noise = np.array(W, dtype=float)
        W_noise += 1e-20 * np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        L_noise = -W_noise
        np.fill_diagonal(L_noise, W_noise.sum(axis=1))
        g = gsp.UndirectedWeightedGraph(L=L_noise, enforce_symmetric=False)
        assert g._checked_symmetric
        assert not g._enforced_symmetric
        assert (g.L == L_noise).all()
        assert (g.W == W_noise).all()

        W_noise = np.array(W, dtype=float)
        W_noise += np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        L_noise = -W_noise
        np.fill_diagonal(L_noise, W_noise.sum(axis=1))
        g = gsp.UndirectedWeightedGraph(L=L_noise, check_symmetric=False)
        assert not g._checked_symmetric
        assert g._enforced_symmetric
        assert (g.L == g.L.T).all()
        assert (g.W == g.W.T).all()

        W_noise = np.array(W, dtype=float)
        W_noise += np.random.random_sample(W_noise.shape)
        np.fill_diagonal(W_noise, 0)
        L_noise = -W_noise
        np.fill_diagonal(L_noise, W_noise.sum(axis=1))
        g = gsp.UndirectedWeightedGraph(L=L_noise, check_symmetric=False,
                                        enforce_symmetric=False)
        assert not g._checked_symmetric
        assert not g._enforced_symmetric
        assert (g.L == L_noise).all()
        assert (g.W == W_noise).all()

        W_noise = np.array(W, dtype=float)
        W_noise[0, 3] = -1e-200
        W_noise[3, 0] = -1e-200
        L_noise = -W_noise
        np.fill_diagonal(L_noise, W_noise.sum(axis=1))
        with pytest.raises(gsp.GraphBadParametersError,
                           match='L should.*n.*negative'):
            gsp.UndirectedWeightedGraph(L=L_noise)

        W_noise = np.array(W, dtype=float)
        W_noise[0, 3] = -1
        W_noise[3, 0] = -1
        L_noise = -W_noise
        np.fill_diagonal(L_noise, W_noise.sum(axis=1))
        g = gsp.UndirectedWeightedGraph(L=L_noise, check_weight_signal=False)

        W_noise = np.asarray(W, dtype=float)
        W_noise[0, 3] = -1
        W_noise[3, 0] = -1
        g = gsp.UndirectedWeightedGraph(L=L_noise, check_weight_signal=False)
        assert not g._checked_weight_signals
        assert g.W[0, 3] < 0
        assert g.L[0, 3] > 0

    def test_init_other(self):

        assert gsp.UndirectedWeightedGraph.ev_matrix is \
            gsp.UndirectedWeightedGraph.U

        with pytest.raises(gsp.GraphBadParametersError,
                           match='W must.*bi-dimens'):
            gsp.UndirectedWeightedGraph(W=[0, 1, 1, 2, 3, 5, 8])

        W = [[0, 1, 2, 0],
             [1, 0, 0, 0],
             [2, 0, 0, 3],
             [0, 0, 3, 0]]

        g = gsp.UndirectedWeightedGraph(W=W)
        with pytest.raises(ValueError, match='read.only'):
            g.L[0, 0] = 0
        assert all(n == i for i, n in enumerate(g.nodes))

        with pytest.raises(gsp.GraphBadParametersError,
                           match='length.*nodes'):
            gsp.UndirectedWeightedGraph(W=W, nodes='abc')

        nodes = 'abcd'
        g = gsp.UndirectedWeightedGraph(W=W, nodes=nodes)
        assert all(n == c for n, c in zip(g.nodes, nodes))

        assert not g._spectrum_cached
        assert not g._W_is_persistent

    def test_spectrum(self):

        W = [[0, 1, 2, 0],
             [1, 0, 0, 0],
             [2, 0, 0, 3],
             [0, 0, 3, 0]]

        g = gsp.UndirectedWeightedGraph(W=W, precalc_spectrum=True)
        assert g._spectrum_cached
        assert hasattr(g, '_U') and hasattr(g, '_spectrum')

        del g.spectrum
        assert not g._spectrum_cached

        U = g.U
        assert g._spectrum_cached
        assert U is g.U
        assert U is g.ev_matrix
        assert g.spectrum is g.spectrum

        del g.spectrum
        g.delete_cached_spectral_decomposition()
        del g.spectrum
        lamb = g.spectrum
        assert lamb is g.spectrum
        with pytest.raises(ValueError, match='read.only'):
            lamb[0] = -1

        del g.spectrum
        assert lamb == pytest.approx(g.diag_lambda().diagonal())
        assert g._spectrum_cached

        del g.spectrum
        lamb2, U2 = g.spectral_decomposition()
        with pytest.raises(ValueError, match='read.only'):
            U[0, 0] = -1
        assert g._spectrum_cached
        assert lamb2 == pytest.approx(lamb)
        assert U2 == pytest.approx(U)

    def test_W(self):

        W = np.array(
            [[0, 1, 2, 0],
             [1, 0, 0, 0],
             [2, 0, 0, 3],
             [0, 0, 3, 0]], dtype=float)

        g = gsp.UndirectedWeightedGraph(W=W)
        assert g.W == pytest.approx(W)
        assert not g._W_is_persistent
        assert (g.W.diagonal() == np.zeros((len(W),))).all()
        assert not g._W_is_persistent

        _W = g.make_W_persistent()
        assert _W == pytest.approx(W)
        assert g._W_is_persistent
        assert hasattr(g, '_W')
        assert g.W is g._W
        assert g.W is _W
        with pytest.raises(ValueError, match='read.only'):
            g.W[0, 3] = 10
        assert g.make_W_persistent() is _W
        assert g.adjacency is _W

        del g.W
        assert not g._W_is_persistent
        del g.adjacency
        assert not g._W_is_persistent  # shouldn't blow up

    def test_L(self):

        W = np.array(
            [[0, 1, 2, 0],
             [1, 0, 0, 0],
             [2, 0, 0, 3],
             [0, 0, 3, 0]], dtype=float)
        L = -W
        np.fill_diagonal(L, W.sum(axis=1))

        g = gsp.UndirectedWeightedGraph(W=W)

        assert g.L is g.laplacian
        assert g.L == pytest.approx(L)

        assert len(g) == len(W)

    def test_edges(self):

        W = np.array(
            [[0, 1, 2, 0],
             [1, 0, 0, 0],
             [2, 0, 0, 3],
             [0, 0, 3, 0]], dtype=float)

        edges = gsp.UndirectedWeightedGraph(W=W).edges
        assert len(edges) == 3
        assert all((i, j) in edges or (j, i) in edges
                   for i, j in [(0, 1), (0, 2), (2, 3)])

    def test_behaviour(self):

        W = np.array(
            [[0, 2, 1, 0],
             [2, 0, 0, 0],
             [1, 0, 0, 2],
             [0, 0, 2, 0]], dtype=float)
        g = gsp.UndirectedWeightedGraph(W=W)
        ew, U = g.spectral_decomposition()

        expected_ew = np.array([0, 3-np.sqrt(5), 4, 3+np.sqrt(5)])
        assert ew == pytest.approx(expected_ew)

        phi = (np.sqrt(5) - 1) / 2
        expected_U = np.array([
            [1, 1, 1, 1],
            [-phi, -1, phi, 1],
            [1, -1, 1, -1],
            [1, -phi, -1, phi],
        ]).T
        expected_U /= np.linalg.norm(expected_U, axis=0, keepdims=True)
        for i in range(len(g)):
            assert expected_U[:, i] == pytest.approx(g.U[:, i]) or \
                expected_U[:, i] == pytest.approx(-g.U[:, i])


class TestGeoUWG:

    _W = np.array([
        [0, 2, 1, 0],
        [2, 0, 0, 0],
        [1, 0, 0, 2],
        [0, 0, 2, 0]], dtype=float)
    _points = np.array([
        [1, 1],
        [0, 1],
        [0, 0],
        [1, 0]], dtype=float)

    def test_init(self):

        _L = -self._W
        np.fill_diagonal(_L, self._W.sum(axis=1))

        with pytest.raises(gsp.GraphBadParametersError, match='none'):
            gsp.GeometricUWG(self._points)

        def populate_edges(points, W=self._W):
            return W

        with pytest.raises(gsp.GraphBadParametersError, match='two or more'):
            gsp.GeometricUWG(
                self._points,
                populate_edges=populate_edges,
                W=self._W)
        with pytest.raises(gsp.GraphBadParametersError, match='two or more'):
            gsp.GeometricUWG(
                self._points,
                populate_edges=populate_edges,
                L=_L)

        g = gsp.GeometricUWG(self._points, populate_edges=populate_edges)
        assert g.W == pytest.approx(self._W)
        assert g.L == pytest.approx(_L)

        with pytest.raises(gsp.GraphBadParametersError, match='same length'):
            gsp.GeometricUWG(
                self._points,
                populate_edges=lambda points: np.array([[0, 1], [1, 2]]))

        g = gsp.GeometricUWG(self._points, W=self._W)
        assert g.W == pytest.approx(self._W)
        assert g.L == pytest.approx(_L)

        g = gsp.GeometricUWG(self._points, L=_L)
        assert g.W == pytest.approx(self._W)
        assert g.L == pytest.approx(_L)

        with pytest.raises(gsp.GraphBadParametersError, match='at most one'):
            gsp.GeometricUWG(self._points, W=self._W, L=_L)

    def test_random(self):

        with pytest.raises(gsp.GraphBadParametersError, match='provided none'):
            gsp.GeometricUWG.make_random()

        g = gsp.GeometricUWG.make_random(N=50)
        assert len(g) == 50

        g = gsp.GeometricUWG.make_random(points=self._points)
        assert (g.nodes == self._points).all()

        g = gsp.GeometricUWG.make_random(
            N=1, random_point_factory=lambda: np.array([0, 0]))
        assert g.L.shape == (1, 1)
        assert g.L[0, 0] == 0
        assert (g.nodes[0] == np.array([0, 0])).all()

        isk = gsp.GeometricUWG.make_invsquare_kernel()
        g = gsp.GeometricUWG.make_random(
            points=self._points, kernel=isk)
        assert g.L[0, 1] / g.L[0, 2] == pytest.approx(2)

        scale = 10 * np.random.random_sample()
        factor = 10 * np.random.random_sample()
        dist = 100 * np.random.random_sample()
        isk0 = gsp.GeometricUWG.make_invsquare_kernel(scale=scale)
        isk1 = gsp.GeometricUWG.make_invsquare_kernel(scale=scale*factor)
        assert isk0(dist) == pytest.approx(isk1(factor * dist))

        gk = gsp.GeometricUWG.make_gauss_kernel(sigma=scale)
        assert gk(scale*10) == pytest.approx(0)
        assert gk(0) == 1

        points3d = np.array([
            [0, 0, 0],
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
            [1, 1, 1]], dtype=float)
        isk = gsp.GeometricUWG.make_invsquare_kernel()
        g = gsp.GeometricUWG.make_random(points=points3d, kernel=isk)
        expected_invW = np.array([
            [0, 1, 1, 1, 3],
            [1, 0, 2, 2, 2],
            [1, 2, 0, 2, 2],
            [1, 2, 2, 0, 2],
            [3, 2, 2, 2, 0]], dtype=float)
        np.fill_diagonal(expected_invW, 1)
        expected_W = 1 / expected_invW
        np.fill_diagonal(expected_W, 0)
        assert expected_W == pytest.approx(g.W)

        with pytest.raises(gsp.GraphBadParametersError, match='provided both'):
            gsp.GeometricUWG.make_random(N=10, points=self._points)

        with pytest.raises(gsp.GraphBadParametersError,
                           match='planar.*d.*=.*2'):
            gsp.GeometricUWG.make_random(points=points3d, planar=True)

    @staticmethod
    def check_planar(g):
        edges = g.edges
        for i in range(len(edges)):
            for j in range(i+1, len(edges)):
                e0 = g.nodes[edges[i][0]]
                e1 = g.nodes[edges[i][1]]
                f0 = g.nodes[edges[j][0]]
                f1 = g.nodes[edges[j][1]]
                assert not gsp.GeometricUWG._edges_intersect(
                    (e0, e1), (f0, f1), thresh=100e-16)

    def test_pretty(self):

        # TODO: 200 takes way too long. Figure out why, fix it, and make the
        #       tests work (fast) for 200, 500 and 1000.
        for N in [10, 20, 50, 100]:
            gsp.GeometricUWG.make_random_pretty(N=N)

        self.check_planar(gsp.GeometricUWG.make_random_pretty('planar'))
        self.check_planar(gsp.GeometricUWG.make_random_pretty('planar2'))
        with pytest.raises(AssertionError):
            self.check_planar(
                gsp.GeometricUWG.make_random_pretty(Wthresh=1e-3))

        with pytest.raises(gsp.GraphBadParametersError, match='(?i)unknown'):
            gsp.GeometricUWG.make_random_pretty('sdfgfdsdgscgd')

    def test_degenerate(self):

        self.check_planar(gsp.GeometricUWG.make_random(
            points=self._points,
            planar=True))

        g = gsp.GeometricUWG.make_random(
            points=np.array([[0, 0], [1, 0], [2, 0], [3, 0]], dtype=float),
            planar=True)
        self.check_planar(g)
        expected_W = np.array([
            [0, 1, 0, 0],
            [1, 0, 1, 0],
            [0, 1, 0, 1],
            [0, 0, 1, 0]], dtype=float)
        assert (g.W == expected_W).all()

        assert not gsp.GeometricUWG._edges_intersect(
            (np.array([0., 0.]), np.array([0., 0.])),
            (np.array([0., 0.]), np.array([0., 1.])),
            thresh=100e-16)
