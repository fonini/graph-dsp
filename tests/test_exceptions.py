#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


"""
Test module for the exceptions defined by the Graph Signal Processing package
"""


import pytest

import graphdsp.core as gsp_core


class TestGE:

    def test_raise(self):
        with pytest.raises(gsp_core.GSPError):
            raise gsp_core.GSPError

    def test_subclass(self):
        assert issubclass(gsp_core.GSPError, Exception)


class TestGBPE:

    def test_subclass(self):
        assert issubclass(gsp_core.GraphBadParametersError, gsp_core.GSPError)
