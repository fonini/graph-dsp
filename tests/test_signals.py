#
# Universidade Federal do Rio de Janeiro
# Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa
# Programa de Engenharia Elétrica
# Signals, Multimedia, and Telecommunications group
# (SMT / PEE / COPPE / UFRJ)
#
# Author: Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
# Advisors: Paulo Sergio Ramirez Diniz
#           Markus Vinícius Santos Lima
#


"""
Test module for graph signals
"""


import pytest

import numpy as np
from scipy import linalg

import graphdsp as gsp
from graphdsp.core.signals import GraphSignalBase


class TestGSBase:

    def test_abstract(self):
        with pytest.raises(TypeError, match="Can't instantiate abstract"):
            GraphSignalBase([])


class TestGSConcrete:

    def test_basic(self):

        values = [-3, 2, -1, 1, 0, 1, 1, 2, 3, 5, 8]

        for cls in [gsp.GraphSignal, gsp.GraphSignalSpectrum]:

            # from List, do Not copy
            values_ln = values[:]
            signal = cls(values_ln, copy_values=False)
            assert signal.graph is None
            assert all(val == sigval for val, sigval in zip(values_ln, signal))
            assert not signal._transform_is_cached

            # from List, do (Yes) copy
            values_ly = values[:]
            signal = cls(values_ly)
            assert signal[3] == values_ly[3]
            values_ly[3] += 100
            assert signal[3] + 100 == values_ly[3]

            # from Array, do Not copy
            values_an = np.array(values[:], dtype=float)
            signal = cls(values_an, copy_values=False, mutable=False)
            assert signal[3] == values_an[3]
            assert np.may_share_memory(signal._values, values_an)
            with pytest.raises(ValueError, match='read-only'):
                values_an[3] += 100

            # from Array, do (Yes) copy
            values_ay = np.array(values[:], dtype=float)
            signal = cls(values_ay)
            assert signal[3] == values_ay[3]
            values_ay[3] += 100
            assert signal[3] + 100 == values_ay[3]

        # generate a random orthogonal matrix U
        randmat = np.random.standard_normal(tuple([len(values)]*2))
        W = np.abs((randmat + randmat.T) / 2)
        np.fill_diagonal(W, 0)
        L = -W
        np.fill_diagonal(L, W.sum(axis=1))
        __, U = linalg.eigh(L)

        cls = gsp.GraphSignal
        transf_values = U.T @ values
        signal = cls(values, from_transform=transf_values)
        assert isinstance(signal.gft, cls._transform_class)
        assert len(signal) == len(signal.gft)
        assert signal._transform_is_cached
        assert all(v == sv for v, sv in zip(transf_values, signal.gft))
        assert signal.gft._transform_is_cached
        assert signal.gft.igft is signal
        with pytest.raises(ValueError, match='already has a transform'):
            cls(values, from_transform=signal.gft)

        cls = gsp.GraphSignalSpectrum
        fd, td = cls.make_transform_pair(transf_values, values)
        assert td is fd.igft
        assert fd is td.gft
        assert isinstance(fd, cls)
        assert isinstance(td, cls._transform_class)
        assert len(fd) == len(td)
        assert all(d._transform_is_cached for d in [td, fd])
        assert all(v == sv for v, sv in zip(transf_values, fd))
        assert all(v == sv for v, sv in zip(values, td))
        assert td.gft.igft is td
        assert fd.igft.gft is fd
        with pytest.raises(ValueError, match='already has a transform'):
            cls(values, from_transform=td)
        td.break_reference_loop()
        assert not td._transform_is_cached
        assert not fd._transform_is_cached

    def test_graph(self):

        g = gsp.GeometricUWG.make_random_pretty()
        values = np.random.standard_normal((len(g),))

        with pytest.raises(gsp.GraphBadParametersError,
                           match='number.*equal'):
            gsp.GraphSignal(values[:-1], graph=g)

        x = gsp.GraphSignal(values)
        x.graph = g
        assert (x.values == values).all()

    def test_spectrum(self):

        W = np.array(
            [[0, 2, 1, 0],
             [2, 0, 0, 0],
             [1, 0, 0, 2],
             [0, 0, 2, 0]], dtype=float)
        g = gsp.UndirectedWeightedGraph(W=W)

        phi = (np.sqrt(5) - 1) / 2
        expected_U = np.array([
            [1, 1, 1, 1],
            [-phi, -1, phi, 1],
            [1, -1, 1, -1],
            [1, -phi, -1, phi],
        ]).T
        expected_U /= np.linalg.norm(expected_U, axis=0, keepdims=True)

        values = np.random.standard_normal((len(g),))
        x = gsp.GraphSignal(values, g)
        assert x.graph is g
        assert x.gft[0] == pytest.approx(values.mean() * np.sqrt(len(g)))
        for i in range(1, len(g)):
            assert np.abs(x.gft[i]) == \
                pytest.approx(np.abs(expected_U[:, i].dot(x)))

        assert x.gft is g.gft(x)

        fvalues = x.gft.values.copy()
        del g.spectrum
        assert x.values == pytest.approx(g.igft(fvalues).values)

        del x.graph
        with pytest.raises(ValueError, match='transform.*graph.*known'):
            x.gft
        with pytest.raises(ValueError, match='nodes.*equal.*values'):
            x.graph = gsp.GeometricUWG.make_random_pretty(N=len(x) + 1)

        del x.gft
        assert not x._transform_is_cached

        f = gsp.GraphSignalSpectrum(fvalues, g)
        assert f.igft.values == pytest.approx(x.values)
