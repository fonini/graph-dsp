Graph Signal Processing
============================

http://www.pee.ufrj.br/index.php/pt/producao-academica/dissertacoes-de-mestrado/2019-1/2016033357-a-didactic-introduction-to-graph-signal-processing-techniques-and-applications/file

Testing
-------

Lint and test without coverage:

.. code-block:: bash

    flake8
    tox

Or with coverage:

.. code-block:: bash

    flake8
    tox -- --cov=src --cov-report term-missing
